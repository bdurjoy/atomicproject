<?php
/**
 * Created by PhpStorm.
 * User: durjoy
 * Date: 26-Mar-17
 * Time: 1:10 PM
 */

namespace App\Model;
use PDO, PDOException;


class Database
{
    public $dbh;


    public function __construct()
    {

        try{
            $this->dbh = new PDO('mysql:host=localhost; dbname=atomicproject', "root","");
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $error){

        }
    }
}