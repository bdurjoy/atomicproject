<?php
/**
 * Created by PhpStorm.
 * User: durjoy
 * Date: 08-Apr-17
 * Time: 10:39 PM
 */

namespace App\Birthday;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;


class Birthday extends DB
{
    private $id;
    private $personName;
    private $birthDate;


    public function setData($postData){
        if(array_key_exists('id', $postData)){
            $this->id = $postData['id'];
        }
        if(array_key_exists('name', $postData)){
            $this->personName = $postData['name'];
        }
        if(array_key_exists('birthDate', $postData)){
            $this->birthDate = $postData['birthDate'];
        }
    }

    public function store(){
        $arrData = array($this->personName, $this->birthDate);
        $sql = "INSERT INTO birthdate(person_name, birth_date) VALUES(?,?)";
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        if($result){
            Message::message("<span>Data Inserted Successfully</span>");
        }else{
            Message::message("<span>Sorry! Data is not Inserted</span>");
        }
        Utility::redirect('create.php');
    }

    public function index(){
        $sql = "SELECT * FROM birthdate WHERE soft_deleted='No'";
        $STH = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){
        $sql = "SELECT * From birthdate WHERE soft_deleted='Yes'";
        $STH = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function view(){
        $sql = "SELECT * From birthdate WHERE id='$this->id'";
        $STH = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function update(){
        $arrData = array($this->personName, $this->birthDate);
        $sql = "UPDATE booktitle SET person_name=?, birth_date=? WHERE id=".$this->id;
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("<span>Data Updated Successfully</span>");
        }
        else{
            Message::setMessage("<span>Data Updating Failed</span>");
        }
        Utility::redirect('index.php');
    }

    public function trash(){
        $sql = "UPDATE birthdate SET soft_deleted='Yes' WHERE id=".$this->id;

        $result = $this->dbh->exec($sql);
        if($result){
            Message::setMessage("<span>Data Trashed Successfully</span>");
        }else{
            Message::setMessage("<span>Data does not trashed</span>");
        }
        Utility::redirect('index.php');
    }

    public function recover(){
        $sql = "UPDATE birthdate SET soft_deleted='No' WHERE id=".$this->id;

        $result = $this->dbh->exec($sql);
        if($result){
            Message::setMessage("<span>Data Recovered Successfully</span>");
        }else{
            Message::setMessage("<span>Data does not recovered</span>");
        }
        Utility::redirect('index.php');
    }

    public function delete(){
        $sql = "DELETE from birthdate WHERE id=".$this->id;

        $result = $this->dbh->exec($sql);
        if($result){
            Message::setMessage("<span>Data Parmanently Deleted</span>");
        }else{
            Message::setMessage("<span>Data Has not been parmanently Deleted</span>");
        }
        Utility::redirect('index.php');
    }


    public function indexpaginator($page=1, $itemsPerPage=3){
        try{

            $start = (($page-1)*$itemsPerPage);
            if($start<0) $start=0;

            $sql = "SELECT * FROM birthdate WHERE soft_deleted='No' LIMIT $start, $itemsPerPage";
        }catch(PDOException $error){
            $sql = "SELECT * FROM birthdate WHERE soft_deleted ='No'";
        }
        $STH = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }
    public function trashedpaginator($page=1, $itemsPerPage=3){
        try{
            $start= (($page-1)*$itemsPerPage);
            if($start<0) $start=0;

            $sql = "SELECT * FROM birthdate WHERE soft_deleted='Yes' LIMIT $start, $itemsPerPage";
        }catch(PDOException $error){
            $sql = "SELECT * FROM birthdate WHERE soft_deleted='Yes'";
        }
        $STH = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData = $STH->fetchAll();
        return $arrSomeData;
    }



    public function trashMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "UPDATE  birthdate SET soft_deleted='Yes' WHERE id=".$id;

            $result = $this->dbh->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('trashed.php?Page=1');


    }


    public function recoverMultiple($markArray){


        foreach($markArray as $id){

            $sql = "UPDATE  birthdate SET soft_deleted='No' WHERE id=".$id;

            $result = $this->dbh->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function deleteMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "Delete from birthdate  WHERE id=".$id;

            $result = $this->dbh->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function listSelectedData($selectedIDs){



        foreach($selectedIDs as $id){

            $sql = "Select * from birthdate  WHERE id=".$id;


            $STH = $this->dbh->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $someData[]  = $STH->fetch();


        }


        return $someData;


    }




    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byPerson']) && isset($requestArray['byDate']) )  $sql = "SELECT * FROM `birthdate` WHERE `soft_deleted` ='No' AND (`person_name` LIKE '%".$requestArray['search']."%' OR `birth_date` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byPerson']) && !isset($requestArray['byDate']) ) $sql = "SELECT * FROM `birthdate` WHERE `soft_deleted` ='No' AND `person_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byPerson']) && isset($requestArray['byDate']) )  $sql = "SELECT * FROM `birthdate` WHERE `soft_deleted` ='No' AND `birth_date` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->person_name);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->person_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->birth_date);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->birth_date);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords


}