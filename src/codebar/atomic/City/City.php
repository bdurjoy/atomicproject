<?php
/**
 * Created by PhpStorm.
 * User: durjoy
 * Date: 08-Apr-17
 * Time: 10:39 PM
 */

namespace App\City;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class City extends DB
{
    private $id;
    private $user_name;
    private $district_name;
    private $city_name;

    public function setData($postData){
        if(array_key_exists('id', $postData)){
            $this->id = $postData['id'];
        }
        if(array_key_exists('userName', $postData)){
            $this->user_name = $postData['userName'];
        }
        if(array_key_exists('district', $postData)){
            $this->district_name = $postData['district'];
        }
        if(array_key_exists('cityName', $postData)){
            $this->city_name = $postData['cityName'];
        }
    }

    public function store(){
        $arrData = array($this->user_name, $this->district_name, $this->city_name);
        $sql = "INSERT INTO cityinfo(user_name, district_name, city_name) VALUES(?,?,?)";
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        if($result){
            Message::message("<span>Data Inserted Successfully</span>");
        }else{
            Message::message("<span>Sorry! Data is not Inserted</span>");
        }
        Utility::redirect('create.php');
    }

    public function index(){
        $sql = "SELECT * FROM cityinfo WHERE soft_deleted='No'";
        $STH = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function trashed(){
        $sql = "SELECT * From cityinfo WHERE soft_deleted='Yes'";
        $STH = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function view(){
        $sql = "SELECT * From cityinfo WHERE id='$this->id'";
        $STH = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function update(){
        $arrData = array($this->user_name, $this->district_name, $this->city_name);
        $sql = "UPDATE cityinfo SET user_name=?, district_name=?, city_name=? WHERE id=".$this->id;
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("<span>Data Updated Successfully</span>");
        }
        else{
            Message::setMessage("<span>Data Updating Failed</span>");
        }
        Utility::redirect('index.php');
    }

    public function trash(){
        $sql = "UPDATE cityinfo SET soft_deleted='Yes' WHERE id=".$this->id;

        $result = $this->dbh->exec($sql);
        if($result){
            Message::setMessage("<span>Data Trashed Successfully</span>");
        }else{
            Message::setMessage("<span>Data does not trashed</span>");
        }
        Utility::redirect('index.php');
    }

    public function recover(){
        $sql = "UPDATE cityinfo SET soft_deleted='No' WHERE id=".$this->id;

        $result = $this->dbh->exec($sql);
        if($result){
            Message::setMessage("<span>Data Recovered Successfully</span>");
        }else{
            Message::setMessage("<span>Data does not recovered</span>");
        }
        Utility::redirect('index.php');
    }

    public function delete(){
        $sql = "DELETE from cityinfo WHERE id=".$this->id;

        $result = $this->dbh->exec($sql);
        if($result){
            Message::setMessage("<span>Data Parmanently Deleted</span>");
        }else{
            Message::setMessage("<span>Data Has not been parmanently Deleted</span>");
        }
        Utility::redirect('index.php');
    }


    public function indexpaginator($page=1, $itemsPerPage=3){
        try{

            $start = (($page-1)*$itemsPerPage);
            if($start<0) $start=0;

            $sql = "SELECT * FROM cityinfo WHERE soft_deleted='No' LIMIT $start, $itemsPerPage";
        }catch(PDOException $error){
            $sql = "SELECT * FROM cityinfo WHERE soft_deleted ='No'";
        }
        $STH = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }
    public function trashedpaginator($page=1, $itemsPerPage=3){
        try{
            $start= (($page-1)*$itemsPerPage);
            if($start<0) $start=0;

            $sql = "SELECT * FROM cityinfo WHERE soft_deleted='Yes' LIMIT $start, $itemsPerPage";
        }catch(PDOException $error){
            $sql = "SELECT * FROM cityinfo WHERE soft_deleted='Yes'";
        }
        $STH = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData = $STH->fetchAll();
        return $arrSomeData;
    }



    public function trashMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "UPDATE  cityinfo SET soft_deleted='Yes' WHERE id=".$id;

            $result = $this->dbh->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('trashed.php?Page=1');


    }


    public function recoverMultiple($markArray){


        foreach($markArray as $id){

            $sql = "UPDATE  cityinfo SET soft_deleted='No' WHERE id=".$id;

            $result = $this->dbh->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function deleteMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "Delete from cityinfo  WHERE id=".$id;

            $result = $this->dbh->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function listSelectedData($selectedIDs){



        foreach($selectedIDs as $id){

            $sql = "Select * from cityinfo  WHERE id=".$id;


            $STH = $this->dbh->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $someData[]  = $STH->fetch();


        }


        return $someData;


    }




    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byCity']) )  $sql = "SELECT * FROM `cityinfo` WHERE `soft_deleted` ='No' AND (`user_name` LIKE '%".$requestArray['search']."%' OR `city_name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byCity']) ) $sql = "SELECT * FROM `cityinfo` WHERE `soft_deleted` ='No' AND `user_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byCity']) )  $sql = "SELECT * FROM `cityinfo` WHERE `soft_deleted` ='No' AND `city_name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->user_name);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->user_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->district_name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->district_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords

}