<?php
require_once ("../../../vendor/autoload.php");

use App\Birthday\Birthday;
use App\Message\Message;

$objBirthdate = new Birthday();
$objBirthdate->setData($_GET);
$oneData = $objBirthdate->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birthdate - Edit Form</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/main.css">
    <script src="../../../resources/js/jquery-3.1.1.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>
</head>
<body>
<div class="container bg-1">
    <h1>Birthdate - Edit Form</h1>
    <section>
        <nav class="navbar">
            <div class="menu">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="../">Home</a></li>
                    <li class="active"><a href="index.php">Index</a></li>
                    <li class="active"><a href="trashed.php">Trashed List</a></li>
                </ul>
            </div>
        </nav>
    </section>
    <div class="form-create text-center">
        <form class="form-group" action="update.php" method="post">
            <h2>Person Name: </h2>
            <input type="hidden" name="id" value="<?php echo $oneData->id?>">
            <input class="" type="text" name="bookName" value="<?php echo $oneData->person_name?>">
            <h2>Birth Date: </h2>
            <input class="" type="text" name="authorName" value="<?php echo $oneData->birth_date?>">
            <br>
            <input class="btn btn-primary" type="submit">
        </form>
    </div>
</div>

<script src="../../../resources/js/jquery.js"></script>
<script>
    jQuery(function($) {
        $('.message').fadeOut(550);
        $('.message').fadeIn(500);
        $('.message').fadeOut(550);
        $('.message').fadeIn(500);
        $('.message').fadeOut(550);
    })
</script>
</body>
</html>
