<?php
require_once "../../../vendor/autoload.php";

use App\Birthday\Birthday;
use App\Message\Message;
use App\Utility\Utility;


if(isset($_POST['mark'])){
    $objBirthdate = new Birthday();

    $objBirthdate->trashMultiple($_POST['mark']);
    Utility::redirect('trashed.php?Page=1');
}
else{
    Message::message("No data Selected to trash");
    Utility::redirect("index.php");
}