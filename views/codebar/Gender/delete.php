<?php
require_once("../../../vendor/autoload.php");

$objBookTitle = new \App\BookTitle\BookTitle();
$objBookTitle->setData($_GET);
$oneData = $objBookTitle->view();

if(isset($_GET['YesButton'])) $objBookTitle->delete();

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title - Delete</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/main.css">
    <script src="../../../resources/js/jquery.js"></script>
    <script src="../../../resources/js/jquery-3.1.1.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>

</head>
<body>


<div class="container bg-1">
    <h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Book Name</th>
            <th>Author Name</th>
        </tr>

        <?php

        echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->book_name</td>
                     <td>$oneData->author_name</td>

                  </tr>
              ";

        ?>

    </table>

    <?php
    echo "
          <a href='delete.php?id=$oneData->id&YesButton=1' class='btn btn-danger'>Yes</a>

          <a href='index.php' class='btn btn-success'>No</a>
        ";

    ?>
</div>



<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>