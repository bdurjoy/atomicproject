<?php
include_once ("../../../vendor/autoload.php");


$objEmail = new App\Email\Email();

$allData = $objEmail->index();


use App\Message\Message;
use App\Utility\Utility;


if(!isset($_SESSION)) session_start();
$msg = Message::getMessage();



################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$someData =  $objEmail->search($_REQUEST);
$availableKeywords=$objEmail->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################




//-------pagination block------//
$recordCount= count($allData);
if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objEmail->indexpaginator($page,$itemsPerPage);

$serial = (  ($page-1) * $itemsPerPage ) +1;

if($serial<1) $serial=1;
//-------pagination block------//




################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objEmail->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Active List - Email</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/main.css">
    <script src="../../../resources/js/jquery.js"></script>
    <script src="../../../resources/js/jquery-3.1.1.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>
</head>
<body>
<div class="container bg-1">
    <h1>Active Email List</h1>
    <nav class="navbar">
        <div class="menu">
            <ul class="nav navbar-nav">
                <li class="active"><a href="../">Home</a></li>
                <li class="active"><a href="create.php">New Entry</a></li>
                <li class="active"><a href="trashed.php">Trashed List</a></li>
            </ul>
        </div>
        <form id="searchForm" action="index.php" class="search"  method="get">
            <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
            <input type="checkbox"  name="byName"   checked  >By Name
            <input type="checkbox"  name="byEmail"  checked >By Email
            <input hidden type="submit" class="btn-primary" value="search">
        </form>
    </nav>



    <form action="trashmultiple.php" method="post" id="multiple">
    <div class="navbar btn-group">
        <button type="button" class="btn btn-danger" id="delete">Delete  Selected</button>&nbsp;&nbsp;&nbsp;
        <button type="submit" class="btn btn-warning">Trash Selected</button>
        <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
        <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
        <a href="email.php?list=1" class="btn btn-primary" role="button">Email Active List To A friend</a>

    </div>
    <div class="message"><?php echo $msg?></div>




        <table class="table table-striped">

            <tr class="bg-3">
                <th class="limit">Select All<br><input id="select_all" type="checkbox" value="select all"></th>
                <th class="limit">SL</th>
                <th>ID</th>
                <th class="limit">User Name</th>
                <th class="limit">Email Address</th>
                <th>Action Buttons</th>
            </tr>
            <?php
            //$serial=1;
                foreach ($someData as $oneData) {

                    if ($serial % 2) {
                        $bgColor = "#1b6d85";
                    } else {
                        $bgColor = "#555555";
                    }
                    echo "
            <tr style='background-color: $bgColor' class='bg-4'>
                <td style='padding-left: 6%'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
                <td>$serial</td>
                <td>$oneData->id</td>
                <td>$oneData->user_name</td>
                <td>$oneData->email_add</td>
                <td><div class='btn-group btn-group-sm' role='group'><a href='view.php?id=$oneData->id' class='btn btn-primary'>View</a>
                <a href='edit.php?id=$oneData->id' class='btn btn-success'>Update</a>
                <a href='trash.php?id=$oneData->id' class='btn btn-warning'>Soft Delete</a></div></td>
            </tr>
        ";
                    $serial++;

                }
            ?>

        </table>
        </form>

<div align="left">
    <ul class="pagination">
        <?php

        $pageMinusOne  = $page-1;
        $pagePlusOne  = $page+1;


        if($page>$pages) Utility::redirect("index.php?Page=$pages");

        if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";


        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

        }
        if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6" selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10" selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15" selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
</div>





<script>
    jQuery(function($) {
        $('.message').fadeOut(550);
        $('.message').fadeIn(500);
        $('.message').fadeOut(550);
        $('.message').fadeIn(500);
        $('.message').fadeOut(550);
    })

    $('#delete').on('click',function(){
        document.forms[1].action="deletemultiple.php";
        $('#multiple').submit();
    });



    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });

    $(function(){
        var availableTags = [
            <?php
            echo $comma_separated_keywords;
            ?>
        ];

        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });
    });

</script>
</body>
</html>
