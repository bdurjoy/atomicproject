<?php
require_once "../../../vendor/autoload.php";

use App\Email\Email;
use App\Message\Message;
use App\Utility\Utility;


if(isset($_POST['mark'])){
    $objEmail = new Email();

    $objEmail->trashMultiple($_POST['mark']);
    Utility::redirect('trashed.php?Page=1');
}
else{
    Message::message("No data Selected to trash");
    Utility::redirect("index.php");
}