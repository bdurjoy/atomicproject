<?php
include_once ('../../../vendor/autoload.php');
use App\Email\Email;

$obj= new Email();
$recordSet=$obj->index();
$trs="";
$sl=0;

foreach($recordSet as $row) {
    $id =  $row->id;
    $userName = $row->user_name;
    $emailAdd =$row->email_add;

    $sl++;
    $trs .= "<tr>";
    $trs .= "<td width='100'> $sl</td>";
    $trs .= "<td width='100'> $id </td>";
    $trs .= "<td width='250'> $userName </td>";
    $trs .= "<td width='250'> $emailAdd </td>";

    $trs .= "</tr>";
}

$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >User Name</th>
                    <th align='left' >Email Address</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>


BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('Email_Info.pdf', 'D');