<?php
include_once ("../../../vendor/autoload.php");


$objEmail = new App\Email\Email();
use App\Message\Message;
use App\Utility\Utility;


if(!isset($_SESSION)) session_start();

$allData = $objEmail->trashed();


//------pagination block------//


$recordCount= count($allData);
if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objEmail->trashedpaginator($page,$itemsPerPage);

$serial = (  ($page-1) * $itemsPerPage ) +1;

if($serial<1) $serial=1;
//------pagination block------//
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Trashed List - Email Info</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/main.css">
    <script src="../../../resources/js/jquery.js"></script>
    <script src="../../../resources/js/jquery-3.1.1.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>
</head>
<body>
<div class="container bg-1">
    <h1>Inactive Email List</h1>
    <nav class="navbar">
        <div class="menu">
            <ul class="nav navbar-nav">
                <li class="active"><a href="../">Home</a></li>
                <li class="active"><a href="create.php">New Entry</a></li>
                <li class="active"><a href="index.php">index</a></li>
            </ul>
        </div>
        <form id="searchForm" action="index.php" class="search"  method="get">
            <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
            <input type="checkbox"  name="byName"   checked  >By Name
            <input type="checkbox"  name="byEmail"  checked >By Email
            <input hidden type="submit" class="btn-primary" value="search">
        </form>
    </nav>





    <table class="table table-striped">

        <tr class="bg-3">
            <th class="limit">Select All<br><input id="select_all" type="checkbox" value="select all"></th>
            <th class="limit">SL</th>
            <th>ID</th>
            <th class="limit">User Name</th>
            <th class="limit">Email Address</th>
            <th>Action Buttons</th>
        </tr>
        <?php
        //$serial=1;
        foreach ($someData as $oneData) {

            if ($serial % 2) {
                $bgColor = "#1b6d85";
            } else {
                $bgColor = "#555555";
            }
            echo "
            <tr style='background-color: $bgColor' class='bg-4'>
                <td style='padding-left: 6%'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
                <td>$serial</td>
                <td>$oneData->id</td>
                <td>$oneData->user_name</td>
                <td>$oneData->email_add</td>
                <td><div class='btn-group btn-group-sm' role='group'><a href='view.php?id=$oneData->id' class='btn btn-primary'>View</a>
                <a href='recover.php?id=$oneData->id' class='btn btn-sm btn-info'>Recover</a>
                <a href='delete.php?id=$oneData->id' class='btn btn-sm btn-danger'>Delete</a></div></td>
            </tr>
        ";
            $serial++;

        }
        ?>

    </table>


<div align="left" class="container">
    <ul class="pagination">
        <?php
        $pageMinusOne = $page-1;
        $pagePlusOne = $page+1;

        if($page>$pages) Utility::redirect("index.php?Page=$pages");

        if($page>1) echo "<li><a href='index.php?Page=$pageMinusOne'>Previous</a></li>";

        for($i = 1; $i <=$pages; $i++){
            if($i == $page) echo '<li class="active"><a href="">'.$i.'</a></li>';
            else echo "<li><a href='?Page=$i'>".$i.'</a></li>';
        }
        if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>Next</a></li>";

        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
</div>




</body>
</html>
