<?php
require_once ("../../../vendor/autoload.php");

use App\Message\Message;

$objCity = new App\City\City();
$objCity->setData($_GET);
$oneData = $objCity->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City Info - Edit Form</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/main.css">
    <script src="../../../resources/js/jquery-3.1.1.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>
</head>
<body>
<div class="container bg-1">
    <h1>City Info - Edit Form</h1>
    <section>
        <nav class="navbar">
            <div class="menu">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="../">Home</a></li>
                    <li class="active"><a href="index.php">Index</a></li>
                    <li class="active"><a href="trashed.php">Trashed List</a></li>
                </ul>
            </div>
        </nav>
    </section>
    <div class="form-create text-center">
        <form class="form-group" action="update.php" method="post">
            <h2>Person Name: </h2>
            <input type="hidden" name="id" value="<?php echo $oneData->id?>">
            <input class="" type="text" name="userName" value="<?php echo $oneData->user_name?>">

            <h2>City Name: </h2>
            <input class="" type="text" name="cityName" value="<?php echo $oneData->city_name?>">
            <br>


            <div>
                <select name="district" class="btn btn-default">
                    <option value="">District</option>
                    <option value="Dhaka" <?php if($oneData->district_name=="Dhaka") echo "selected"?>>Dhaka</option>
                    <option value="Chittagong" <?php if($oneData->district_name=="Chittagong") echo "selected"?>>Chittagong</option>
                    <option value="Rajshahi" <?php if($oneData->district_name=="Rajshahi")echo "selected"?>>Rajshahi</option>
                    <option value="Khulna" <?php if($oneData->district_name=="Khulna") echo "selected"?>>Khulna</option>
                    <option value="Shyllet" <?php if($oneData->district_name=="Shyllet") echo "selected"?>>Shyllet</option>
                    <option value="Barisal" <?php if($oneData->district_name=="Barisal") echo "selected"?>>Barisal</option>
                    <option value="Moymensingh" <?php if($oneData->district_name=="Moymensingh") echo "selected"?>>Moymensingh</option>
                </select>
            </div>
            <input class="btn btn-primary" type="submit">
        </form>
    </div>
</div>

<script src="../../../resources/js/jquery.js"></script>
<script>
    jQuery(function($) {
        $('.message').fadeOut(550);
        $('.message').fadeIn(500);
        $('.message').fadeOut(550);
        $('.message').fadeIn(500);
        $('.message').fadeOut(550);
    })
</script>
</body>
</html>
