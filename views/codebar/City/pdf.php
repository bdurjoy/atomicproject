<?php
include_once ('../../../vendor/autoload.php');

use App\City\City;

$obj= new City();
$recordSet=$obj->index();
$trs="";
$sl=0;

foreach($recordSet as $row) {
    $id =  $row->id;
    $userName = $row->user_name;
    $cityName =$row->city_name;
    $districtName =$row->district_name;

    $sl++;
    $trs .= "<tr>";
    $trs .= "<td width='100'> $sl</td>";
    $trs .= "<td width='100'> $id </td>";
    $trs .= "<td width='250'> $userName</td>";
    $trs .= "<td width='250'> $cityName</td>";
    $trs .= "<td width='250'> $districtName</td>";

    $trs .= "</tr>";
}

$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >Person Name</th>
                    <th align='left' >City Name</th>
                    <th align='left' >District Name</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>


BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('Cityinfo.pdf', 'D');