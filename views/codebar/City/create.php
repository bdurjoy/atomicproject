<?php
require_once ("../../../vendor/autoload.php");

use App\City\City;
use App\Message\Message;

if(!isset($_SESSION)) session_start();
$msg = Message::message();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City Info - Insert Form</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/main.css">
    <script src="../../../resources/js/jquery-3.1.1.js"></script>
    <script src="../../../resources/js/bootstrap.min.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>
</head>
<body>
<div class="container bg-1">
    <h1>City - Insert Form</h1>
        <section>
        <nav class="navbar">
            <div class="menu">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="../">Home</a></li>
                    <li class="active"><a href="index.php">Index</a></li>
                    <li class="active"><a href="trashed.php">Trashed List</a></li>
                </ul>
            </div>
        </nav>
    </section>
    <h4 class="message"><?php echo $msg?></h4>
    <div class="form-create text-center">
    <form class="form-group" action="store.php" method="post">
        <h2>Person Name</h2>
        <input type="text" name="userName" placeholder="User Name">

        <div>
        <select name="district" class="btn btn-default">
            <option value="">District</option>
            <option value="Dhaka">Dhaka</option>
            <option value="Chittagong">Chittagong</option>
            <option value="Rajshahi">Rajshahi</option>
            <option value="Khulna">Khulna</option>
            <option value="Shyllet">Shyllet</option>
            <option value="Barisal">Barisal</option>
            <option value="Moymensingh">Moymensingh</option>
        </select>
        </div>
        <h2>City Name: </h2>
        <input class="" type="text" name="cityName" placeholder="City Name">
        <br>
        <input class="btn btn-primary" type="submit">
    </form>
        </div>
</div>

<script src="../../../resources/js/jquery.js"></script>
<script>
    jQuery(function($) {
        $('.message').fadeOut(550);
        $('.message').fadeIn(500);
        $('.message').fadeOut(550);
        $('.message').fadeIn(500);
        $('.message').fadeOut(550);
    })
</script>
</body>
</html>
