<?php
require_once ("../../../vendor/autoload.php");


use App\Message\Message;

$objCity = new App\City\City();
$objCity->setData($_GET);

$oneData = $objCity->view();


if(!isset($_SESSION)){
    session_start();
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City Info - View Page</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/main.css">
    <script src="../../../resources/js/jquery-3.1.1.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>
</head>
<body>
<div class="container bg-1">
    <h1>City - Single View</h1>
    <nav class="navbar">
        <div class="menu">
            <ul class="nav navbar-nav">
                <li class="active"><a href="../">Home</a></li>
                <li class="active"><a href="index.php">Index</a></li>
                <li class="active"><a href="trashed.php">Trashed List</a></li>
            </ul>
        </div>
    </nav>

    <?php echo "<h1>ID: $oneData->id <br>
    $oneData->user_name From: $oneData->city_name, $oneData->district_name</h1>


"?>
</div>


</body>
</html>