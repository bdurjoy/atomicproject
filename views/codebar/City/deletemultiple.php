<?php

require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
session_start();

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City Info - Multiple Deletation</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/main.css">
    <script src="../../../resources/js/jquery.js"></script>
    <script src="../../../resources/js/bootstrap.min.js"></script>


</head>
<body>



<div class="container bg-1">



    <?php

    if(isset($_POST['mark']) || isset($_SESSION['mark'])) {    // start of boss if
        $someData=null;
        $objCity = new App\City\City();


        if(isset($_POST['mark']) ){
            $_SESSION['mark'] = $_POST['mark'];
            $someData =  $objCity->listSelectedData($_SESSION['mark']);
        }
        echo "
          <h1> Are you sure you want to delete all selected data?</h1>
          <a href='deletemultiple.php?YesButton=1' class='btn btn-danger'>Yes</a>

          <a href='index.php' class='btn btn-success'>No</a>
        ";


        if(isset($_GET['YesButton'])){
            $objCity->deleteMultiple($_SESSION['mark']);
            unset($_SESSION['mark']);
        }
        ?>


        <table class="table table-striped">


            <tr>


                <th style='width: 10%; text-align: center'>Serial Number</th>
                <th style='width: 10%; text-align: center'>ID</th>
                <th>Person Name</th>
                <th>City Name</th>
                <th>District Name</th>
            </tr>

            <?php
            $serial = 1;


            foreach ($someData as $oneData) { ########### Traversing $someData is Required for pagination  #############

                if ($serial % 2) $bgColor = "#1b6d85";
                else $bgColor = "#555555";

                echo "

                  <tr  style='background-color: $bgColor' class='bg-4'>


                     <td style='width: 10%; text-align: center'>$serial</td>
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->user_name</td>
                     <td>$oneData->city_name</td>
                     <td>$oneData->district_name</td>


                  </tr>
              ";
                $serial++;
            }
            ?>

        </table>

        <?php
    }  // end of boos if
    else
    {
        Message::message("Empty Selection! Please select some records.");
        Utility::redirect($_SERVER["HTTP_REFERER"]);
    }


    ?>


</div>


</body>
</html>