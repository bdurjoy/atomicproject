<?php
require_once("../../../vendor/autoload.php");

$objCity = new App\City\City();
$objCity->setData($_GET);
$oneData = $objCity->view();

if(isset($_GET['YesButton'])) $objCity->delete();

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City Info - Delete</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/main.css">
    <script src="../../../resources/js/jquery.js"></script>
    <script src="../../../resources/js/jquery-3.1.1.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>

</head>
<body>


<div class="container bg-1">
    <h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Person Name</th>
            <th>City</th>
            <th>District</th>
        </tr>

        <?php

        echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->user_name</td>
                     <td>$oneData->city_name</td>
                     <td>$oneData->district_name</td>

                  </tr>
              ";

        ?>

    </table>

    <?php
    echo "
          <a href='delete.php?id=$oneData->id&YesButton=1' class='btn btn-danger'>Yes</a>

          <a href='index.php' class='btn btn-success'>No</a>
        ";

    ?>
</div>



<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>