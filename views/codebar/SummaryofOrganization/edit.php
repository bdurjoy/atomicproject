<?php
require_once ("../../../vendor/autoload.php");

use App\Booktitle\Booktitle;
use App\Message\Message;

$objBooktitle = new Booktitle();
$objBooktitle->setData($_GET);
$oneData = $objBooktitle->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title - Edit Form</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/main.css">
    <script src="../../../resources/js/jquery-3.1.1.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>
</head>
<body>
<div class="container bg-1">
    <h1>Book Title - Edit Form</h1>
    <section>
        <nav class="navbar">
            <div class="menu">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="../">Home</a></li>
                    <li class="active"><a href="index.php">Index</a></li>
                    <li class="active"><a href="trashed.php">Trashed List</a></li>
                </ul>
            </div>
        </nav>
    </section>
    <div class="form-create text-center">
        <form class="form-group" action="update.php" method="post">
            <h2>Book Title: </h2>
            <input type="hidden" name="id" value="<?php echo $oneData->id?>">
            <input class="" type="text" name="bookName" value="<?php echo $oneData->book_name?>">
            <h2>Author Name: </h2>
            <input class="" type="text" name="authorName" value="<?php echo $oneData->author_name?>">
            <br>
            <input class="btn btn-primary" type="submit">
        </form>
    </div>
</div>

<script src="../../../resources/js/jquery.js"></script>
<script>
    jQuery(function($) {
        $('.message').fadeOut(550);
        $('.message').fadeIn(500);
        $('.message').fadeOut(550);
        $('.message').fadeIn(500);
        $('.message').fadeOut(550);
    })
</script>
</body>
</html>
